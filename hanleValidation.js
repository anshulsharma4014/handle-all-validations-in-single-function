isValid() {
    let valid = false;
    valid = [
        ...this.template.querySelectorAll("lightning-input"),
        ...this.template.querySelectorAll("lightning-combobox"),
        ...this.template.querySelectorAll("lightning-checkbox-button")
    ].reduce(
        (validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        },
        true
    );
    return valid;
}